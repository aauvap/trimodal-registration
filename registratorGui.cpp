#include "registratorGui.h"

template<typename ... Args>
std::string string_format(const std::string& format, Args ... args)
{
	size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'
	std::unique_ptr<char[]> buf(new char[size]);
	snprintf(buf.get(), size, format.c_str(), args ...);
	return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
}

// **** The remaining code is functions to show images and handle mouse callbacks in OpenCV
void RegistratorGui::markCorrespondingPointsRgb(cv::Point2f rgbCoord)
{
	std::vector<cv::Point2f> vecTCoord,vecDCoord,epiTCoord,vecDistRgbCoord,vecRgbCoord,tempVecRgbCoord;
	cv::Point2f tCoord,dCoord,distRgbCoord;
	vecRgbCoord.push_back(rgbCoord);
	std::vector<int> homInd;

	// ***** Compute the corresponding point in the depth image ******
	registrator->computeCorrespondingDepthPointFromRgb(vecRgbCoord,vecDCoord);
	dCoord = vecDCoord[0];

	// ***** Compute the corresponding point in the thermal image *****
	registrator->computeCorrespondingThermalPointFromRgb(vecRgbCoord, vecTCoord, vecDCoord, homInd, dImg);

	tCoord = vecTCoord[0];

	if (homInd.size() == 0)
	{
		homInd.push_back(0);
	}

	// Mark the points in the RGB, thermal, and depth image
	markCorrespondingPoints(rgbCoord, tCoord, dCoord, homInd[0], MAP_FROM_RGB );

}

void RegistratorGui::markCorrespondingPointsThermal(cv::Point2f tCoord)
{
	std::vector<cv::Point2f> vecTCoord,vecRgbCoord,epiCoord, vecDCoord;
	cv::Point2f rgbCoord, dCoord;
	vecTCoord.push_back(tCoord);
	cv::Mat homEpiToRgb;
	

	// Find the corresponding point in RGB coordinates and process the information to markCorrespondingPoints()
	std::vector<double> minDist; std::vector<int> bestHom;  std::vector<std::vector<int>> octantIndices; std::vector<std::vector<double>> octantDistances;
	registrator->computeCorrespondingRgbPointFromThermal(vecTCoord, vecRgbCoord, minDist, bestHom, octantIndices, octantDistances);
	rgbCoord = vecRgbCoord[0];
	
    registrator->computeCorrespondingDepthPointFromRgb(vecRgbCoord,vecDCoord);
	dCoord = vecDCoord[0];

	if (bestHom.size() == 0)
	{
		bestHom.push_back(0);
	}

	markCorrespondingPoints(rgbCoord, tCoord, dCoord, bestHom[0], MAP_FROM_THERMAL);
	
}

void RegistratorGui::markCorrespondingPointsDepth(cv::Point2f dCoord)
{
	// Find the corresponding point in RGB coordinates and process the information to markCorrespondingPointsRgb()
	cv::Point2f rgbCoord, tCoord;
	std::vector<cv::Point2f> vecDCoord,vecRgbCoord, vecTCoord;
	vecDCoord.push_back(dCoord);
	
	registrator->computeCorrespondingRgbPointFromDepth(vecDCoord, vecRgbCoord);
	rgbCoord = vecRgbCoord[0];
	
	// ***** Compute the corresponding point in the thermal image *****
	std::vector<int> bestHom;

	registrator->computeCorrespondingThermalPointFromRgb(vecRgbCoord, vecTCoord, vecDCoord, bestHom, dImg);
	tCoord = vecTCoord[0];

	markCorrespondingPoints(rgbCoord, tCoord, dCoord, bestHom[0], MAP_FROM_DEPTH);
}

void RegistratorGui::markCorrespondingPoints(cv::Point2f rgbCoord, cv::Point2f tCoord, cv::Point2f dCoord, int homInd, int MAP_TYPE)
{
	cv::Scalar rgbColor(0,0,255);
	cv::Scalar tColor(255,255,255);
	cv::Scalar dColor(65535,65535,65535);
	

	// Mark the points in the RGB, thermal, and depth image
	try {
		cv::Mat tmpRgbImg = rgbImg.clone();
		cv::Mat tmpTImg = tImg.clone();
		cv::Mat tmpDDimg = dImg.clone();

		circle(tmpRgbImg, rgbCoord, 2, rgbColor, -1);
		imshow("RGB", tmpRgbImg);

		circle(tmpTImg, tCoord, 2, tColor, -1);
		imshow("Thermal", tmpTImg);

		circle(tmpDDimg, dCoord, 2, dColor, -1);
		imshow("Depth", tmpDDimg);
	}
	catch (cv::Exception& e) {
		std::cerr << "Unable to show images " << e.what() << std::endl;
	}

	float depthInMm;
	// Find the depth value of the point and write it in the status window
	if ((dCoord.y > 0) && (dCoord.x > 0) && (dCoord.y <= registrator->getCalibrationParams().HEIGHT) 
			&& (dCoord.x <= registrator->getCalibrationParams().WIDTH))
	{
		depthInMm = registrator->lookUpDepth(dImg, dCoord, true);
	} else {
		depthInMm = 0;
	}


	// Write the coordinates in the status field
	std::string statusMsg = string_format("RGB: %3.0f,%3.0f :: Depth: %3.0f,%3.0f :: Thermal: %3.0f,%3.0f :: Depth of point: %4.1f mm :: Using hom #%d",rgbCoord.x,rgbCoord.y,dCoord.x,dCoord.y,tCoord.x,tCoord.y,depthInMm,homInd);

	try
	{
		cv::displayStatusBar("RGB", statusMsg, 0);
	} 
	catch (cv::Exception& e)
	{
		std::cout << "Unable to show information in status bar. Make sure that OpenCV is compiled with Qt support" << std::endl;
	}
	
}

void RegistratorGui::updateFrames(int imgNbr, void* obj)
{
	RegistratorGui* reg = (RegistratorGui*) obj; // Recast
	
	if (imgNbr == 0) {
		imgNbr = 1; 
		cv::setTrackbarPos("Frame","RGB",imgNbr);
	}
	reg->showModalityImages(reg->registrator->getRgbImgPath(),
                            reg->registrator->getTImgPath(),
                            reg->registrator->getDImgPath(),imgNbr);
}

void RegistratorGui::showModalityImages(std::string rgbPath,std::string tPath,std::string dPath,int imgNbr)
{
	std::string rgbImgStr,tImgStr,dImgStr;
	cv::Mat distTImg,distRgbImg,rectTImg,rectRgbImg;
	
	// Insert the number of leading zeros
	std::stringstream nbrStr;
	nbrStr << std::setfill('0') << std::setw(5) << imgNbr;

	rgbImgStr = rgbPath + nbrStr.str() + ".jpg";
	tImgStr = tPath + nbrStr.str() + ".jpg";
	dImgStr = dPath + nbrStr.str() + ".png";

	distTImg = cv::imread(tImgStr, CV_LOAD_IMAGE_GRAYSCALE); 
    distRgbImg = cv::imread(rgbImgStr);  // Read thermal and rgb images
	
	calibrationParams stereoCalibParams = registrator->getCalibrationParams();

	try
	{
		if (registrator->getRegistrationSettings().UNDISTORT_IMAGES)
		{
			undistort(distTImg,rectTImg,stereoCalibParams.tCamMat,stereoCalibParams.tDistCoeff);
			undistort(distRgbImg,rectRgbImg,stereoCalibParams.rgbCamMat,stereoCalibParams.rgbDistCoeff);
			tImg = rectTImg;
			rgbImg = rectRgbImg;
		} else{
			tImg = distTImg;
			rgbImg = distRgbImg;
		}
	} 
	catch (cv::Exception& e){
		    std::cerr << "Unable to load images " << e.what() << std::endl;
	}

	try {
	cv::imshow("Thermal",tImg); 
    cv::imshow("RGB",rgbImg); // Show thermal and rgb images
	
    dImg = cv::imread(dImgStr,CV_LOAD_IMAGE_ANYDEPTH); 
    imshow("Depth",dImg); // Read and show the depth image
	} catch (cv::Exception& e){
		std::cerr << "Unable to show images " << e.what() << std::endl;
	}
	
}

void RegistratorGui::rgbOnMouse( int event, int x, int y, int)
{
	if( event != CV_EVENT_LBUTTONDOWN )
        return;

	cv::Point2i rgbCoord;
	rgbCoord.x = x;
	rgbCoord.y = y;
		
	markCorrespondingPointsRgb(rgbCoord);
}

void RegistratorGui::thermalOnMouse( int event, int x, int y, int)
{
	if( event != CV_EVENT_LBUTTONDOWN )
        return;

	cv::Point2i tCoord;
	tCoord.x = x;
	tCoord.y = y;

	markCorrespondingPointsThermal(tCoord);

}

void RegistratorGui::depthOnMouse( int event, int x, int y, int)
{
	if( event != CV_EVENT_LBUTTONDOWN )
        return;

	cv::Point2i dCoord;
	dCoord.x = x;
	dCoord.y = y;
	markCorrespondingPointsDepth(dCoord);

}

int RegistratorGui::initWindows(int imgNbr)
{
	// Create windows
	cv::namedWindow("RGB",CV_WINDOW_AUTOSIZE);
	cv::namedWindow("Thermal",CV_WINDOW_AUTOSIZE);
	cv::namedWindow("Depth",CV_WINDOW_AUTOSIZE);

	// Get number of images in rgb folder
	std::vector<std::string> rgbIndices;

	registrator->buildImageDirectory(registrator->getRgbImgPath(), rgbIndices, ".jpg");
	int count = int(rgbIndices.size());

	if (count == 0)
	{
		std::cout << "Did not find any (.jpg) images at " << registrator->getRgbImgPath() << " - exiting" << std::endl;
		return 1;
	}
	
	// Create trackbar
	cv::createTrackbar("Frame","RGB",&imgNbr,count, updateFrames, this);
	showModalityImages(registrator->getRgbImgPath(), 
                       registrator->getTImgPath(), 
                       registrator->getDImgPath(),imgNbr);

	return 0;
}

void wrappedRgbOnMouse(int event, int x, int y, int flags, void* ptr)
{
    RegistratorGui* mcPtr = (RegistratorGui*)ptr;
    if(mcPtr != NULL)
        mcPtr->rgbOnMouse(event, x, y, flags);
}

void wrappedThermalOnMouse(int event, int x, int y, int flags, void* ptr)
{
    RegistratorGui* mcPtr = (RegistratorGui*)ptr;
    if(mcPtr != NULL)
        mcPtr->thermalOnMouse(event, x, y, flags);
}

void wrappedDepthOnMouse(int event, int x, int y, int flags, void* ptr)
{
    RegistratorGui* mcPtr = (RegistratorGui*)ptr;
    if(mcPtr != NULL)
        mcPtr->depthOnMouse(event, x, y, flags);
}

int main(int argc, char *argv[])
{	
	std::string scenePath;

	if (argc < 2 || argc > 3) {
		std::cerr << "Usage : registratorGui.exe path\\to\\scene\\of\\trimodal\\dataset\\";
		return 0;
	}
	else {
		scenePath = argv[1];
	}


	std::cout << "\n scenePath: " << scenePath << std::endl;

	// Create an instance of the registration class
    RegistratorGui registratorGui;

    registratorGui.registrator = std::make_unique<Registrator>(
                        scenePath + "/calibVars.yml", 
                        scenePath + "/SyncRGB/",
						scenePath + "/SyncD/", 
						scenePath + "/SyncT/");

	// Are the input coordinates undistorted, or are we working on the original, distorted images? 
	// If input coordinates are distorted, this property should be set to false
	registratorGui.registrator->toggleUndistortion(true);

	// When registering contours, this property should be set to true - otherwise, it should be false
	registratorGui.registrator->setUsePrevDepthPoint(false);


	// Show the synchronized imagery in three different windows of OpenCV:
	int imgNbr = 1;
	int retVal = registratorGui.initWindows(imgNbr); 

	if (retVal != 0)
	{
		return 0;
	}

	// Wrapper for handling tbe mouse events to the registrator class
	cv::setMouseCallback("RGB", wrappedRgbOnMouse, (void*)&registratorGui);
	cv::setMouseCallback("Thermal", wrappedThermalOnMouse, (void*)&registratorGui);
	cv::setMouseCallback("Depth", wrappedDepthOnMouse, (void*)&registratorGui);
	
	// Run forever
	while (true)
	{
		cvWaitKey(-1);
	}

	return 0;
}