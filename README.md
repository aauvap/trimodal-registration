## Helper utilities for the AAU VAP Trimodal People segmentation dataset

This repository hosts the source code for interacting with the AAU VAP Trimodal People segmentation dataset.
The dataset is available from https://www.kaggle.com/aalborguniversity/trimodal-people-segmentation/

Here you will find the registration and visualization code for the dataset. It has been placed on Bitbucket to allow quick release of bug fixes and improvements without updating the entire dataset on Kaggle.

### Contents
- **registrator.cpp**, **registrator.h**: The core code for registrating points, contours, and images in the dataset from RGB <-> Thermal, RGB <-> Depth, and Thermal <-> RGB <-> Depth. Requires C++17 and OpenCV 3.x or greater.
- **registratorGui.cpp**, **registratorGui.h**: Demonstration code that enables the user to click on a point in one modality (e.g. RGB) and see the corresponding registered point in another modality (e.g. thermal). Precompiled binaries for Windows are found in the *RegistrationDemoBinaries* folder.
- **Inspect COCO annotations.ipynb**: Jupyter Notebook that demonstrates how to visualize the annotated masks of the dataset.
- **exportGtToCOCO.py**: Python script that converts the annotated masks to MSCOCO-compatible format. Might be used if additional annotations should be converted.
