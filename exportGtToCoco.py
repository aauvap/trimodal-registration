# Export ground truth annotations from the AAU Trimodal People Segmentation Dataset
# to be compatible with the MSCOCO json format

import copy
import cv2
import datetime
import json
import numpy as np
import os

import bbTools

def imageToDictEntry(imagePath, imageId, shortPath):
    imageFile = cv2.imread(imagePath)

    image = dict()

    if imageFile is not None:
        height, width, channels = imageFile.shape

        # Image dictionary
        image = dict()
        image['id'] = imageIdCounter
        image['width'] = width
        image['height'] = height
        image['file_name'] = shortPath
        image['license'] = 0
        #image['date_captured'] = datetime.datetime(2017, 3, 23)
    else:
        print("Could not read: " + imagePath)

    return image

def annotationLineToDictEntry(line, imageId, annotationId):
    entries = line.split(';')

    if len(entries) < 7:
        return None


    cocoCatName = 'person' # We only have persons in the trimodal set
    cocoCatId = cocoCategoryNumbers[cocoCatName]

    annotation = dict() 
    annotation["original_grayscale_value"] = int(entries[6])
    annotation['id'] = annotationId
    annotation['image_id'] = imageId
    annotation['category_id'] = cocoCatId
    
    #annotation['area'] = w * h
    #annotation['bbox'] = [int(round(uLCx)), int(round(uLCy)), int(round(w)), int(round(h))]
    annotation['bbox'] = [0, 0, 0, 0]
    annotation['iscrowd'] = 0

    return annotation

def getPolygonFromImageMask(imagePath, bbox, grayScaleValue):
    
    image = cv2.imread(imagePath, cv2.IMREAD_GRAYSCALE)

    if image is None:
        return []

    largestId = -1
    largestArea = 0
    candidateId = -1
    candidateArea = 0
    correctedBbox = bbox
    correctBbox = True


    # Segment the id of the general mask - the mask in the resulting image
    # will have pixel value 255
    idMask = cv2.inRange(image, grayScaleValue, grayScaleValue)

    area = int(np.sum(idMask == 255))

        
    # Convert this mask to a polygon
    contours, hierarchy = cv2.findContours(idMask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    if correctBbox:
        upperLeftCornerX = 1000
        upperLeftCornerY = 1000
        lowerRightCornerX = 0
        lowerRightCornerY = 0

        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)

            if x < upperLeftCornerX:
                upperLeftCornerX = x

            if y < upperLeftCornerY:
                upperLeftCornerY = y

            if lowerRightCornerX < (x + w):
                lowerRightCornerX = x + w

            if lowerRightCornerY < (y + h):
                lowerRightCornerY = y + h

        newW = lowerRightCornerX - upperLeftCornerX
        newH = lowerRightCornerY - upperLeftCornerY

        correctedBbox = [upperLeftCornerX, upperLeftCornerY, newW, newH]


    if contours is None:
        return [None , None, None]

    # Flatten and convert the contour to a list such that it can 
    # be serialized into JSON-format
    flatContoursList = []
    for contour in contours:
        flatContourList = []
        for point in contour:
            for coord in point[0]:
                flatContourList.append(int(coord))

        flatContoursList.append(flatContourList)

    return [flatContoursList, area, correctedBbox]


def createCocoCategoryList(categoryLabels):
    categories = []

    for catId, name in categoryLabels.items():
        catEntry = dict()
        catEntry['id'] = catId
        catEntry['name'] = name
        catEntry['supercategory'] = 'None'

        categories.append(catEntry)

    return categories



# Import category names list

# # Create an image ID list with unique ID's for the entire trimodal dataset such that
# # the corresponding dections may be coupled with the correspoinding image

trimodalPath = #PATH_TO_DATASET
scenes = sorted(os.listdir(trimodalPath))

modalities = ['rgb', 'thermal', 'depth']
modalityImageTranslator = dict(rgb='SyncRGB', thermal='SyncT', depth='SyncD')

images = dict()
annotations = dict()
idToImageLookupTable = dict()
imageToIdLookupTable = dict()

for modality in modalities:
    images[modality] = []
    annotations[modality] = []
    idToImageLookupTable[modality] = dict()
    imageToIdLookupTable[modality] = dict()


imageIdCounter = -1
annotationIdCounter = 0
cocoCategoryLabels, cocoCategoryNumbers = bbTools.importCocoCategoryNames()
fullCocoCategories = createCocoCategoryList(cocoCategoryLabels)

categories = []


for scene in scenes:
    scenePath = os.path.join(trimodalPath, scene)

    if not os.path.isdir(scenePath):
        continue

    if ((not os.path.isdir(os.path.join(scenePath, 'rgbMasks'))) or
       (not os.path.isdir(os.path.join(scenePath, 'depthMasks'))) or
       (not os.path.isdir(os.path.join(scenePath, 'thermalMasks')))):
       continue

    if not os.path.isfile(os.path.join(scenePath, 'annotations.csv')):
        continue

    maskFiles = sorted(os.listdir(os.path.join(scenePath, 'rgbMasks')))

    for maskFile in maskFiles:
        if not '.png' in maskFile:
            continue

        maskPath = dict()

        for modality in modalities:
            maskPath[modality] = os.path.join(scenePath, modality + 'Masks', maskFile)

            if not os.path.isfile(maskPath[modality]):
                continue

        imageIdCounter += 1 

        maskImage = dict()

        for modality in modalities:
            if 'rgb' in modality or 'thermal' in modality:
                actualModalityImage = str(scene) + '/' + modalityImageTranslator[modality] + '/' + str(maskFile).replace('.png', '.jpg')
            else:
                actualModalityImage = str(scene) + '/' + modalityImageTranslator[modality] + '/' + str(maskFile)

            maskImage[modality] = imageToDictEntry(maskPath[modality], 
                                                   imageIdCounter, 
                                                   actualModalityImage)
            images[modality].append(maskImage[modality])

            idToImageLookupTable[modality][imageIdCounter] = actualModalityImage
            imageToIdLookupTable[modality][actualModalityImage] = imageIdCounter

        
    with open(os.path.join(scenePath, 'annotations.csv'), 'r') as annFile:
        firstLine = True
        imageId = 0

        for line in annFile:
            if firstLine:
                firstLine = False
                continue

            columns = line.split(';')

            if len(columns) < 7:
                continue

            filePath = dict()
            filePath['rgb'] = os.path.join(scenePath, columns[0])
            filePath['depth'] = os.path.join(scenePath, columns[1])
            filePath['thermal'] = os.path.join(scenePath, columns[2])

            actualModalityImage = dict()

            
            actualModalityImage['rgb'] = str(scene) + '/' + columns[3].replace('./', '').replace('.png', '.jpg')            
            actualModalityImage['depth'] = str(scene) + '/' +  columns[4].replace('./', '')
            actualModalityImage['thermal'] = str(scene) + '/' +  columns[5].replace('./', '')

            for modality in modalities:
                if os.path.isfile(filePath[modality]):
                    
                    if actualModalityImage[modality] in imageToIdLookupTable[modality]:
                        imageId = imageToIdLookupTable[modality][actualModalityImage[modality]]                            
                    else:
                        print("Image at " + actualModalityImage[modality] + " not found in dict!")
                else:
                    print("Image at " + filePath[modality] + " not found!")

            try:
                annotation = annotationLineToDictEntry(line, 
                                                       imageId,
                                                       annotationIdCounter)
            except KeyError:
                print("Trouble with key in file:" + scenePath + '/' + line)
                      

            if annotation:
                originalAnnotation = copy.deepcopy(annotation)

                for modality in modalities:
                    annotation = copy.deepcopy(originalAnnotation)
                    # Get the segmentation as polygon
                    polygons, area, bbox = getPolygonFromImageMask(filePath[modality], 
                                                            annotation["bbox"],
                                                            annotation["original_grayscale_value"])

                    if polygons is not None:
                        annotation['segmentation'] = polygons
                        annotation['iscrowd'] = 0

                    if area is not None:
                        annotation['area'] = area

                    if bbox is not None:
                        annotation['bbox'] = bbox
                    
                    if (len(annotation['segmentation']) > 0):
                        annotations[modality].append(annotation)

                annotationIdCounter += 1


for modality in modalities:
    gtDict = dict(info = 'AAU VAP Trimodal People Segmentation Dataset - ' + modality, 
                images = images[modality], 
                annotations = annotations[modality],
                licenses = ['CC BY-SA 4.0'],
                categories = fullCocoCategories)

    with open('trimodal-' + modality + '.json', 'w') as outfile:
        json.dump(gtDict, outfile)

lookupDict = dict(idToImageLookupTable = idToImageLookupTable,
                  imageToIdLookupTable = imageToIdLookupTable)

with open('rainSnowHelperDirs.json', 'w') as outfile:
    json.dump(lookupDict, outfile)