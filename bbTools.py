def importYolo9000CategoryNames():
    # Import category names list
    categoryLabels = dict()
    categoryNumbers = dict()

    with open('./9k.names') as f:
        lineNumber = 0

        for line in f:
            entries = line.replace('\n', '')

            if len(entries) > 1:
                number = lineNumber
                label = entries

                categoryLabels[int(number)] = label
                categoryNumbers[label] = int(number)

            lineNumber += 1

    return (categoryLabels, categoryNumbers)

def importCocoCategoryNames():
    # Import category names list
    categoryLabels = dict()
    categoryNumbers = dict()

    with open('./coco.names') as f:
        lineNumber = 1

        for line in f:
            entries = line.replace('\n', '')

            if len(entries) > 1:
                number = lineNumber
                label = entries

                categoryLabels[int(number)] = label
                categoryNumbers[label] = int(number)

            lineNumber += 1

    return (categoryLabels, categoryNumbers)

def yolo9000ToCocoCategories():
    yolo9000ToCoco = dict()

    with open('./yolo9000toCocoCategories.csv') as f:
        for line in f:
            entries = line.replace('\n', '').split(';')

            if len(entries) > 1:
                yolo9000ToCoco[entries[0]] = entries[1]

    return yolo9000ToCoco


def convertYoloInputToStdBB(r, imageWidth, imageHeight):
    """
    Convert the bounding box format as required by YOLO input to 
    standard (top left corner, bottom right corner) coordinates

    Parameters
    ----------
    r : tuple
        [normalisedCentreX, normalisedCentreY, normalisedHeight, normalisedWidth]
    imageWidth : widht of the image in which the coordinates reside, in pixels
    imageHeight : height of the image in which the coordinates reside, in pixels

    
    Returns
    -------
    dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    """

    if r:
        normCentreX, normCentreY, normW, normH = r
        uLCx = int(normCentreX * float(imageWidth) - (normW*float(imageWidth))/2.)
        uLCy = int(normCentreY * float(imageHeight) - (normH * float(imageHeight))/2.)
        lRCx = int(normCentreX * float(imageWidth) + (normW*float(imageWidth))/2.)
        lRCy = int(normCentreY * float(imageHeight) + (normH * float(imageHeight))/2.)

        stdBB = dict(x1 = uLCx, y1 = uLCy, x2 = lRCx, y2 = lRCy)

        return stdBB
    else:
        return dict() 

def parseYOLOInput(line):
    if line:
        labelId, normCentreX, normCentreY, normHeight, normWidth = line.split(' ')

        return (labelId, normCentreX, normCentreY, normHeight, normWidth)
    else: 
        return None    
    


def convertYoloOutputToStdBB(r):
    """
    Convert the bounding box format as output by the YOLO detector to 
    standard (top left corner, bottom right corner) coordinates

    Parameters
    ----------
    r : tuple
        (centreX, centreY, height, width)

    
    Returns
    -------
    dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    """

    if r:
        centrex, centrey, w, h = coord
        uLCx = int(centrex - w/2.)
        uLCy = int(centrey - h/2.)
        lRCx = int(centrex + w/2.)
        lRCy = int(centrey + h/2.)

        stdBB = dict(x1 = uLCx, y1 = uLCy, x2 = lRCx, y2 = lRCy)

        return stdBB
    else:
        return dict()
    
    
    
    
    
    
    

def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    #assert bb1['x1'] < bb1['x2']
    #assert bb1['y1'] < bb1['y2']
    #assert bb2['x1'] < bb2['x2']
    #assert bb2['y1'] < bb2['y2']
    bb1['x1'] < bb1['x2']
    bb1['y1'] < bb1['y2']
    bb2['x1'] < bb2['x2']
    bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou