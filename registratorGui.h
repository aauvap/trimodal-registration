// This source is subject to the MIT Licence
// Copyright (c) 2019 Aalborg University
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// </copyright>
// <author>Chris Bahnsen</author>
// <email>cb@create.aau.dk</email>
// <email2>chris.bahnsen@hotmail.com</email2>
// <date>2019-04-23</date>
// <summary>Contains a point-and-click framework for demonstrating the registration
//	         capabilities of the AAU VAP Trimodal People Segmentation dataset

#ifndef REGISTRATORGUI_H
#define REGISTRATORGUI_H

#include "registrator.h"
#include <memory>
#include <iostream>
#include <string>
#include <cstdio>

class RegistratorGui
{
public:
	void showModalityImages(const std::string rgbPath,
							const std::string tPath,
							const std::string dPath,int imgNbr);
	int initWindows(int imgNbr);
    
    // Mouse handling of OpenCV HighGUI
	void rgbOnMouse( int event, int x, int y, int flags);
	void thermalOnMouse( int event, int x, int y, int flags);
	void depthOnMouse( int event, int x, int y, int flags);

    std::unique_ptr<Registrator> registrator;

	static void updateFrames(int imgNbr, void* obj);

private:
    /* markCorrespondingPoints takes as input a coordinate of the corresponding modality and marks the registered point 
		in the remaining modalities*/
	void markCorrespondingPointsRgb(cv::Point2f rgbCoord);

	void markCorrespondingPointsThermal(cv::Point2f tCoord);

	void markCorrespondingPointsDepth(cv::Point2f dCoord);

	void markCorrespondingPoints(cv::Point2f rgbCoord, cv::Point2f tCoord, cv::Point2f dCoord, int homInd, int MAP_TYPE);


	cv::Mat rgbImg, tImg, dImg;

};

// Declare function prototypes outside of the Registrator class
void wrappedRgbOnMouse(int event, int x, int y, int flags, void* ptr);
void wrappedThermalOnMouse(int event, int x, int y, int flags, void* ptr);
void wrappedDepthOnMouse(int event, int x, int y, int flags, void* ptr);

#endif // REGISTRATORUTILITIES_H
